import { Component, OnInit } from '@angular/core';
import {NgForm} from '@angular/forms';
import {AuthServService} from "../../../auth-serv.service";
import {AngularFireDatabase} from "angularfire2/database";
import {AngularFireAuth} from "angularfire2/auth";

@Component({
  selector: 'app-sign-up',
  templateUrl: './sign-up.component.html',
  styleUrls: ['./sign-up.component.css']
})
export class SignUpComponent implements OnInit {

  constructor(private authServService:AuthServService,
              private afAuth: AngularFireAuth,
              private db: AngularFireDatabase) { }

  ngOnInit() {
  }

  onSup(form: NgForm) {
    const email = form.value.email;
    const password = form.value.password;
    //this.afAuth.auth.createUserWithEmailAndPassword(email, password);
    this.authServService.emailSignUp(email, password);
  }

}
