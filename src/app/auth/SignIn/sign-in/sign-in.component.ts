import { Component, OnInit } from '@angular/core';
import {NgForm} from '@angular/forms';
import {AuthServService} from "../../../auth-serv.service";
import {AngularFireDatabase} from "angularfire2/database";
import {AngularFireAuth} from "angularfire2/auth";


@Component({
  selector: 'app-sign-in',
  templateUrl: './sign-in.component.html',
  styleUrls: ['./sign-in.component.css']
})
export class SignInComponent implements OnInit {

  constructor(private authServService:AuthServService,
              private afAuth: AngularFireAuth,
              private db: AngularFireDatabase) { }

  ngOnInit() {
  }

  onSin(form: NgForm) {
    const email = form.value.email;
    const password = form.value.password;
    this.authServService.emailLogin(email, password);
    console.log("Current user name: " + this.authServService.currentUserId);
  }

  onGoogleSiginIn() {
    this.authServService.googleLogin();
    console.log("Current user name: " + this.authServService.currentUserId);
  }

  onFacebookSiginIn() {
    this.authServService.facebookLogin();
    console.log("Current user id: " + this.authServService.currentUserId);
  }

  onTwitterSiginIn() {
    this.authServService.twitterLogin();
    console.log("Current user id: " + this.authServService.currentUserId);
  }

}
