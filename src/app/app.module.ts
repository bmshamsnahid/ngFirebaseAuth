import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NavigationBarComponent } from './navigation/navigation-bar/navigation-bar.component';
import { SignInComponent } from './auth/SignIn/sign-in/sign-in.component';
import { SignUpComponent } from './auth/SignUp/sign-up/sign-up.component';
import { HomeComponent } from './home/home/home.component';
import { UserCenterComponent } from './home/user/userCenter/user-center/user-center.component';
import { UserDetailComponent } from './home/user/userCenter/userDetail/user-detail/user-detail.component';
import { UserListComponent } from './home/user/userCenter/userList/user-list/user-list.component';
import {FormsModule} from "@angular/forms";
import {HttpModule} from "@angular/http";
import {AuthServService} from "./auth-serv.service";

import { Observable } from 'rxjs/Observable';
import { AngularFireModule } from 'angularfire2';
import { AngularFireDatabaseModule, AngularFireDatabase, FirebaseListObservable } from 'angularfire2/database';
import { AngularFireAuthModule, AngularFireAuth } from 'angularfire2/auth';
import { environment } from '../environments/environment';
import * as firebase from 'firebase/app';

export const firebaseConfig = {
  apiKey: "AIzaSyBniutNpTFQKMXKCH8Znfxwkb4h5Ysyefk",
  authDomain: "ngfirebaseauth-5db75.firebaseapp.com",
  databaseURL: "https://ngfirebaseauth-5db75.firebaseio.com",
  storageBucket: "ngfirebaseauth-5db75.appspot.com",
  messagingSenderId: "707275095205"
};

@NgModule({
  declarations: [
    AppComponent,
    NavigationBarComponent,
    SignInComponent,
    SignUpComponent,
    HomeComponent,
    UserCenterComponent,
    UserDetailComponent,
    UserListComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpModule,
    AngularFireModule.initializeApp(firebaseConfig),

    AngularFireDatabaseModule,
    AngularFireAuthModule
  ],
  providers: [AuthServService],
  bootstrap: [AppComponent]
})
export class AppModule { }
