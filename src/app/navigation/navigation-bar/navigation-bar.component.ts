import { Component, OnInit } from '@angular/core';
import {AuthServService} from "../../auth-serv.service";
import {NgForm} from '@angular/forms';
import {AngularFireDatabase} from "angularfire2/database";
import {AngularFireAuth} from "angularfire2/auth";

@Component({
  selector: 'app-navigation-bar',
  templateUrl: './navigation-bar.component.html',
  styleUrls: ['./navigation-bar.component.css']
})
export class NavigationBarComponent implements OnInit {

  isLoggedIn: boolean;
  visibilityStatusSignIn: boolean;
  visibilityStatusSignUp: boolean;
  displayName: string;

  constructor(private authServService:AuthServService,
              public afAuth: AngularFireAuth) {
    this.afAuth.authState.subscribe(res => {
      if (res && res.uid) {
        this.isLoggedIn = true;
      } else {
        this.isLoggedIn = false;
      }
    });
  }

  ngOnInit() {}

  onClickSignIn() {
    this.visibilityStatusSignIn = !this.visibilityStatusSignIn;
    this.visibilityStatusSignUp = !this.visibilityStatusSignIn;
  }

  onClickSignUp() {
    this.visibilityStatusSignUp = !this.visibilityStatusSignUp;
    this.visibilityStatusSignIn = !this.visibilityStatusSignUp;
  }

  onSin(form: NgForm) {
    const email = form.value.email;
    const password = form.value.password;
    this.authServService.emailLogin(email, password).then(() => {
      this.isLoggedIn =  this.authServService.authenticated;
      if(this.isLoggedIn) {
        this.visibilityStatusSignIn = false;
        this.visibilityStatusSignUp = false;
        this.displayName = this.authServService.currentUserDisplayName;
      }
    });
  }

  onGoogleSiginIn() {
    this.authServService.googleLogin().then(() => {
      this.isLoggedIn =  this.authServService.authenticated;
      if(this.isLoggedIn) {
        this.visibilityStatusSignIn = false;
        this.visibilityStatusSignUp = false;
        this.displayName = this.authServService.currentUserDisplayName;
      }
    });
  }

  onFacebookSiginIn() {
    this.authServService.facebookLogin().then(() => {
      this.isLoggedIn =  this.authServService.authenticated;
      if(this.isLoggedIn) {
        this.visibilityStatusSignIn = false;
        this.visibilityStatusSignUp = false;
        this.displayName = this.authServService.currentUserDisplayName;
      }
    });
  }

  onTwitterSiginIn() {
    this.authServService.twitterLogin().then(() => {
      this.isLoggedIn =  this.authServService.authenticated;
      if(this.isLoggedIn) {
        this.visibilityStatusSignIn = false;
        this.visibilityStatusSignUp = false;
        this.displayName = this.authServService.currentUserDisplayName;
      }
    });
  }

  onSup(form: NgForm) {
    const email = form.value.email;
    const password = form.value.password;
    this.authServService.emailSignUp(email, password).then(() => {
      this.visibilityStatusSignUp = false;
      this.visibilityStatusSignIn = true;
    });
  }

  onSignOut() {
    this.authServService.signOut();
    this.isLoggedIn = false;
  }

}
