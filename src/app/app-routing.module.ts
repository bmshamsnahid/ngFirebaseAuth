import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {HomeComponent} from "./home/home/home.component";
import {SignInComponent} from "./auth/SignIn/sign-in/sign-in.component";
import {SignUpComponent} from "./auth/SignUp/sign-up/sign-up.component";
import {UserCenterComponent} from "./home/user/userCenter/user-center/user-center.component";

const routes: Routes = [
  { path: '', redirectTo: '/home', pathMatch: 'full' },
  { path: 'home', component: HomeComponent },
  { path: 'signIn', component: SignInComponent },
  { path: 'signUp', component: SignUpComponent },
  { path: 'user', component: UserCenterComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
