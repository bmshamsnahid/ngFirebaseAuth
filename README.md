# ngFirebase Authentication

## Instructions

In order to download the code and try it for yourself:

1. Clone the repo: `git clone https://github.com/bmshamsnahid/ngFirebaseAuth.git`
2. Install packages: `npm install`
3. Launch: `ng serve`
4. Make webpack in root/dist: `ng build`
5. Visit in your browser at: `http://localhost:4200`
